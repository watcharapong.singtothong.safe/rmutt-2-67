const express = require('express')
const app = express()
const port = 3000

app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb' }));

const cors = require('cors')
app.use(cors())

const user_route = require('./routes/user')
const login_route = require('./routes/login')


app.use('/api/user', user_route)
app.use('/api/login', login_route)


app.listen(port, () => {
    console.log(' server start at : ' + port);
})