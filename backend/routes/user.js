const express = require('express')
const route = express.Router();

const { MongoClient, ObjectId } = require('mongodb')

const url = "mongodb://rmutt:pAss1234@203.150.107.49:27017/?authMechanism=DEFAULT";

const client = new MongoClient(url)

const database = 'watcharapong'

route.get('/', async (req, res) => {
    await client.connect();
    const db = client.db(database)
    const col = db.collection('user')
    const filter = req.query['filter']
    let query = {}
    if (filter) {
        query['count'] = parseInt(filter)
    }

    let data = await col.find(query).toArray()

    res.json({ status: 'true', data: data })

})

route.post('/create', async (req, res) => {
    await client.connect();
    const db = client.db(database)
    const col = db.collection('user')
    if (!req.body['count']) {
        res.status(400).json({ massge: "ไม่พบ count" })
        return
    }
    let dataInsert = {
        count: req.body['count']
    }
    await col.insertOne(dataInsert)
    res.json({ status: 'true', data: 'สำเร็จ' })
})

route.put('/update/:id', async (req, res) => {
    let id = req.params['id']
    await client.connect();
    const db = client.db(database)
    const col = db.collection('user')
    if (!req.body['count']) {
        res.status(400).json({ massge: "ไม่พบ count" })
        return
    }
    if (id.length < 24) {
        res.status(400).json({ massge: "id ไม่ถูกต้อง" })
        return
    }
    const getData = await col.findOne({ _id: new ObjectId(id) })
    if (!getData) {
        res.status(400).json({ massge: "ไม่พบ id" })
        return
    }
    let dataUpdate = {
        count: req.body['count']
    }
    await col.updateOne({ _id: new ObjectId(id) }, { $set: dataUpdate })
    res.json({ status: 'true', data: 'สำเร็จ' })
})

route.delete('/delete/:id', async (req, res) => {
    let id = req.params['id']
    await client.connect();
    const db = client.db(database)
    const col = db.collection('user')
    const getData = await col.findOne({ _id: new ObjectId(id) })
    if (!getData) {
        res.status(400).json({ massge: "ไม่พบ id" })
        return
    }
    await col.deleteOne({ _id: new ObjectId(id) })
    res.json({ status: 'true', data: 'สำเร็จ' })
})


module.exports = route