const express = require('express')
const route = express.Router();

const { MongoClient, ObjectId } = require('mongodb')

const url = "mongodb://rmutt:pAss1234@203.150.107.49:27017/?authMechanism=DEFAULT";

const client = new MongoClient(url)

const database = 'watcharapong'
function isEmail(email) {
    var emailFormat = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    if (email !== '' && email.match(emailFormat)) { return true; }

    return false;
}
route.post('/login', async (req, res) => {
    await client.connect();
    const db = client.db(database)
    const col = db.collection('accout')
    if (!req.body['username']) {
        res.status(400).json({ massge: "ไม่พบ username" })
        return
    }
    if (!isEmail(req.body['username'])) {
        res.status(400).json({ massge: " username format ไม่ถูกต้อง" })
        return
    }
    if (!req.body['password']) {
        res.status(400).json({ massge: "ไม่พบ password" })
        return
    }

    let data = await col.find({ username: req.body['username'], password: req.body['password'] }).toArray()
    if (data.length == 0) {
        res.status(400).json({ massge: "ไม่พบ user" })
        return
    }
    res.json({ status: 'true', data: data })

})

route.post('/register', async (req, res) => {
    await client.connect();
    const db = client.db(database)
    const col = db.collection('accout')
    if (!req.body['username']) {
        res.status(400).json({ massge: "ไม่พบ username" })
        return
    }
    if (!isEmail(req.body['username'])) {
        res.status(400).json({ massge: " username format ไม่ถูกต้อง" })
        return
    }
    if (!req.body['password']) {
        res.status(400).json({ massge: "ไม่พบ password" })
        return
    }
    let dataInsert = {
        username: req.body['username'],
        password: req.body['password']
    }
    await col.insertOne(dataInsert)
    res.json({ status: 'true', data: 'สำเร็จ' })
})

module.exports = route