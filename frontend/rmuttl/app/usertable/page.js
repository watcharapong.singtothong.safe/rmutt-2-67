
"use client"

import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";

export default function UserTable() {
    const [datatable, setDatatable] = useState([])

    const router = useRouter()


    const getData = () => {
        fetch('http://203.150.107.49/api/user/').then(async (res) => {

            return await res.json()
        }).then((json) => {
            setDatatable(json.data)
        })
    }

    const linktocreatepage = () => {
        router.push('/user')
    }

    const linktoeditpage = (id) => {
        router.push(`/useredit/${id}`)

    }


    useEffect(() => {
        getData()

    }, [])

    return (
        <div className="" style={{ padding: "20px", margin: "20px", border: "2px solid white" }}>
            <div className="flex justify-end">
                <div
                    onClick={() => {
                        linktocreatepage()
                    }}
                    className="border-2 p-3 hover:bg-green-400  cursor-pointer">
                    create count
                </div>
            </div>
            <div className="text-white "> List data</div>
            {datatable.length > 0 && datatable.map((item, index) => {
                return <div
                    onClick={() => {
                        linktoeditpage(item._id)
                    }}
                    key={index} className="grid grid-cols-2 hover:bg-green-400 cursor-pointer">
                    <div> {item._id} </div>
                    <div> {item.count} </div>
                </div>
            })}


        </div>
    )
}