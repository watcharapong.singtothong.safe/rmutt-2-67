"use client"
import Image from "next/image";
import { useState } from "react";
import { useRouter } from "next/navigation"

export default function Home() {
  const [action, setAction] = useState(1)
  const [user, setUser] = useState('')
  const [password, setPassword] = useState('')
  const [ruser, serUser] = useState('')
  const [rpassword, setrPassword] = useState('')
  const [showpassword, setshowPassword] = useState(false)
  const [error, setError] = useState('')
  const router = useRouter()

  const loginData = () => {

    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let body = {
      "username": user,
      "password": password
    }

    const raw = JSON.stringify(body);

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
    };

    fetch("http://203.150.107.49/api/login/login", requestOptions)
      .then(async (response) => await response.json())
      .then((result) => {

        if (result['massge']) {
          setError(result['massge'])
        } else {
          router.push('/usertable')
        }

      })
      .catch((error) => {

        console.error(error)
      });


  }
  const createData = () => {

    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let body = {
      "username": ruser,
      "password": rpassword
    }

    const raw = JSON.stringify(body);

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
    };

    fetch("http://203.150.107.49/api/login/register", requestOptions)
      .then(async (response) => await response.json())
      .then((result) => {

        if (result['massge']) {
          setError(result['massge'])
        } else {
          router.push('/usertable')
        }

      })
      .catch((error) => {

        console.error(error)
      });


  }
  return (
    <div className="flex justify-center items-center min-h-screen">
      <div className="bg-white w-full m-[300px] max-w-[500px] shadow-2xl border-2 rounded-md">
        <div className="flex">
          <div className={`transition-all ${action !== 1 ? 'w-[0px] overflow-hidden' : 'p-20 '}`}>
            <div className="text-black text-[32px] flex items-center">Login
            </div>
            <div className="text-black text-[20px]">User</div>
            <input className="border-2 p-3 text-black"
              value={user}
              onChange={(e) => {
                setUser(e.target.value)
              }}
            ></input>
            <div className="text-black text-[20px] ">Password</div>
            <input className="border-2 p-3 text-black"
              type={!showpassword ? 'password' : 'text'}
              value={password}
              onChange={(e) => {
                setPassword(e.target.value)
              }}
            ></input>
            <div className="text-black">
              <input type="checkbox"
                onClick={() => {
                  setshowPassword(!showpassword)
                }}
              /> show password
            </div>
            <div className="flex gap-3 mt-5">
              <div
                onClick={() => { loginData() }}
                className="btn text-black text-[20px] p-2 border rounded-md cursor-pointer shadow-xl border-green-200 hover:bg-green-200"> submit </div>
              <div
                className="text-black text-[20px] p-2 border rounded-md cursor-pointer shadow-xl border-yellow-200 hover:bg-yellow-200"
                onClick={() => {
                  setAction(2)
                  setError('')
                }}
              > register </div>
            </div>

          </div>
          <div className={`transition-all  ${action !== 2 ? 'w-[0px] overflow-hidden' : 'p-20 '}`}>
            <div className="text-black text-[32px]">Register</div>
            <div className="text-black text-[20px]">User</div>
            <input className="border-2 p-3 text-black"
              value={ruser}
              onChange={(e) => {
                serUser(e.target.value)
              }}
            ></input>
            <div className="text-black text-[20px] ">Password</div>
            <input className="border-2 p-3 text-black"
              value={rpassword}
              onChange={(e) => {
                setrPassword(e.target.value)
              }}
            ></input>

            <div className="flex gap-3 mt-5">
              <div
                onClick={() => { createData() }}
                className="btn text-black text-[20px] p-2 border rounded-md cursor-pointer shadow-xl border-green-200 hover:bg-green-200"> register</div>
              <div
                className="text-black text-[20px] p-2 border rounded-md cursor-pointer shadow-xl border-yellow-200 hover:bg-yellow-200"

                onClick={() => {
                  setAction(1)
                  setError('')
                }}
              > back to login </div>
            </div>

          </div>

        </div>
        {error && <div className="text-[20px]  bg-red-400 p-2 mx-10 rounded-md mt-10 text-center">
          {error}
        </div>}
      </div>
    </div>
  );
}
