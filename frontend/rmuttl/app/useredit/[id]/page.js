
"use client"
import { useEffect, useState } from "react";
import { useRouter, useParams } from "next/navigation"

export default function User() {
    const [data, setData] = useState('')
    const [error, setError] = useState('')
    const router = useRouter()
    const param = useParams()
    const updateData = () => {

        const myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        let body = {
            "count": parseInt(data)
        }

        const raw = JSON.stringify(body);

        const requestOptions = {
            method: "PUT",
            headers: myHeaders,
            body: raw,
        };

        fetch("http://203.150.107.49/api/user/update/" + param.id, requestOptions)
            .then(async (response) => await response.json())
            .then((result) => {

                if (result['massge']) {
                    setError(result['massge'])
                } else {
                    router.push('/usertable')
                }

            })
            .catch((error) => {

                console.error(error)
            });


    }

    const deleteData = () => {

        const requestOptions = {
            method: "DELETE",
        };

        fetch("http://203.150.107.49/api/user/delete/" + param.id, requestOptions)
            .then(async (response) => await response.json())
            .then((result) => {

                if (result['massge']) {
                    setError(result['massge'])
                } else {
                    router.push('/usertable')
                }

            })
            .catch((error) => {

                console.error(error)
            });


    }
    return (
        <div className="" style={{ padding: "20px", margin: "20px", border: "2px solid white" }}>
            <div className="p-[20px]">
                <div className=" flex justify-center text-white">edit count {param.id}</div>

                <div className=" text-red-600"> {error}</div>
                <input
                    onChange={(event) => {
                        setData(event.target.value)
                    }}
                    value={data} className="border-2 text-black" />
                <div className="flex mt-[20px]">
                    <div
                        onClick={() => {
                            updateData()
                        }}
                        className="text-white border-2 p-4 border-green-500 mr-2 cursor-pointer hover:bg-green-500 hover:text-black">
                        submit
                    </div>
                    <div
                        onClick={() => {
                            deleteData()
                        }}
                        className="text-white border-2 p-4 border-red-500 cursor-pointer  hover:bg-red-500 hover:text-black">
                        delete
                    </div>
                </div>
            </div>
        </div>
    )
}